import { useEffect, useState } from "react";
import brand from "../../assets/Frame.png";
const Header = () => {
  const [scrolled, setScrolled] = useState<boolean>(false);
  const [sidebar, setSidebar] = useState(false);

  useEffect(() => {
    if (sidebar) {
      document.body.classList.add("sidebar-active");
    } else {
      document.body.classList.remove("sidebar-active");
    }

    // Cleanup function
    return () => {
      document.body.classList.remove("sidebar-active");
    };
  }, [sidebar]);

  useEffect(() => {
    const handleScroll = () => {
      const scrollTop =
        window.pageYOffset || document.documentElement.scrollTop;
      if (scrollTop > 0) {
        setScrolled(true);
      } else {
        setScrolled(false);
      }
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  useEffect(() => {
    if (scrolled) {
      document.body.classList.add("scrolled");
    } else {
      document.body.classList.remove("scrolled");
    }
  }, [scrolled]);
  return (
    <header className="header">
      <div className="container mx-auto px-5 xl:px-0 py-5">
        <nav className="header-desktop grid grid-cols-2 gap-5">
          <div className="nav-left flex gap-10 items-center  w-full">
            <a href="">
              <img src={brand} alt="" className="max-w-[115px]" />
            </a>
            <ul className="flex items-center justify-between w-full">
              <li>
                <a href="" className="text-primary font-bold">
                  Home
                </a>
              </li>
              <li>
                <a href="" className="text-primary font-bold">
                  Category
                </a>
              </li>
              <li>
                <a href="" className="text-primary font-bold">
                  About
                </a>
              </li>
              <li>
                <a href="" className="text-primary font-bold">
                  Contact
                </a>
              </li>
            </ul>
          </div>
          <div className="nav-right w-full">
            <ul className="flex items-center  justify-between ">
              <li>
                <div className="search-box">
                  <input
                    type="text"
                    className="px-2 py-1 hover:outline-0 focus:outline-0 hover:border-[0] w-full"
                    placeholder="Search something here!"
                  />
                </div>
              </li>
              <li>
                <a href="" className="btn-primary px-8 py-2 ">
                  Join the community
                </a>
              </li>
              <li>
                <button className="flex gap-2 items-center btn-language">
                  <img
                    src="https://i.ibb.co/NWTD3fs/Frame-45.png"
                    className="h-[25px] w-[25px] rounded-full"
                    alt=""
                  />
                  VND
                </button>
              </li>
            </ul>
          </div>
        </nav>
        <nav className="header-mobile flex justify-between items-center gap-5">
          <button onClick={() => setSidebar(!sidebar)}>
            <img
              src="https://i.ibb.co/0ZGLvhF/Vector.png"
              className="h-[25px] w-[25px]"
              alt=""
            />
          </button>
          <a href="">
            <img src={brand} alt="" className="max-w-[115px]" />
          </a>
          <button>
            <img
              src="https://i.ibb.co/9VFWbfC/Vectorrr.png"
              className="h-[25px] w-[25px]"
              alt=""
            />
          </button>
        </nav>
        <div className="sidebar">
          <ul className="flex flex-col gap-7 mt-[50px]">
            <li>
              <a href="" className="text-primary font-bold">
                Home
              </a>
            </li>
            <li>
              <a href="" className="text-primary font-bold">
                Category
              </a>
            </li>
            <li>
              <a href="" className="text-primary font-bold">
                About
              </a>
            </li>
            <li>
              <a href="" className="text-primary font-bold">
                Contact
              </a>
            </li>
            <li>
              <a href="" className="btn-primary px-8 py-2 ">
                Join the community
              </a>
            </li>
            <li>
              <button className="flex gap-2 items-center btn-language">
                <img
                  src="https://i.ibb.co/NWTD3fs/Frame-45.png"
                  className="h-[25px] w-[25px] rounded-full"
                  alt=""
                />
                VND
              </button>
            </li>
          </ul>
        </div>
      </div>
    </header>
  );
};

export default Header;
