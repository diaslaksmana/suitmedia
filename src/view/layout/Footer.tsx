import brand from "../../assets/Frame.png";
const Footer = () => {
  return (
    <footer className="footer rounded-tl-3xl rounded-tr-3xl pt-10">
      <div className="container mx-auto px-5 xl:px-0">
        <div className="footer-top py-10">
          <div className="form-regsiter">
            <div className="block sm:flex items-center gap-5 w-full bg-primary rounded-3xl py-4 px-8">
              <h1 className="text-[24px] font-bold basis-3/12 text-white">
                Register now so you don't miss our programs
              </h1>
              <div className="py-5 sm:p-3 basis-9/12 ">
                <div className="bg-white rounded-xl p-3 block sm:flex w-full items-center gap-3">
                  <input
                    type="text"
                    placeholder="Enter your Email"
                    className=" basis-9/12 border-[#99A2A5] border-[1px] rounded-xl p-4 w-full sm:mb-0 mb-5"
                  />
                  <button className="bg-primary rounded-2xl basis-3/12 p-4 text-white w-full">
                    Subcribe Now
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="nav-bottom block sm:flex justify-between items-center mt-10  border-b border-[#CCD1D2] pb-10">
            <ul className="flex justify-center sm:mb-0 mb-10 gap-5 items-center">
              <li>
                <a href="" className="text-[#00171F] font-medium text-base">
                  Home
                </a>
              </li>
              <li>
                <a href="" className="text-[#00171F] font-medium text-base">
                  Category
                </a>
              </li>
              <li>
                <a href="" className="text-[#00171F] font-medium text-base">
                  About
                </a>
              </li>
              <li>
                <a href="" className="text-[#00171F] font-medium text-base">
                  Contact
                </a>
              </li>
            </ul>
            <ul className="flex gap-5 justify-center items-center">
              <li>
                <a href="">
                  <img
                    src="https://i.ibb.co/PZttYPT/Facebook-Negative.png"
                    alt=""
                    className="h-[25px] w-[25px]"
                  />
                </a>
              </li>
              <li>
                <a href="">
                  <img
                    src="https://i.ibb.co/QNtyJS9/Twitter-Negative.png"
                    alt=""
                    className="h-[25px] w-[25px]"
                  />
                </a>
              </li>
              <li>
                <a href="">
                  <img
                    src="https://i.ibb.co/hB6CJv3/Instagram-Negative.png"
                    alt=""
                    className="h-[25px] w-[25px]"
                  />
                </a>
              </li>
              <li>
                <a href="">
                  <img
                    src="https://i.ibb.co/PgMMSPM/You-Tube-Negative.png"
                    alt=""
                    className="h-[25px] w-[25px]"
                  />
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="footer-bottom pb-10">
          <div className="block sm:flex justify-between items-center">
            <p className="text-[#667479] text-sm font-medium text-center sm:mb-0 mb-5">
              © 2022 Monito. All rights reserved.
            </p>
            <img
              src={brand}
              alt=""
              className="max-w-[115px] mx-auto text-center sm:mb-0 mb-5"
            />
            <ul className="flex items-center gap-5 justify-center">
              <li>
                <a href="" className="text-[#667479] text-sm font-medium ">
                  Terms of Service
                </a>
              </li>
              <li>
                <a href="" className="text-[#667479] text-sm font-medium">
                  Privacy Policy
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
