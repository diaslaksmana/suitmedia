import Footer from "./view/layout/Footer";
import Header from "./view/layout/Header";

function App() {
  return (
    <main>
      <Header />
      <section className="hero rounded-bl-[57px] rounded-br-[57px] relative overflow-hidden">
        <div className="container mx-auto px-5 xl:px-0 ">
          <div className="grid grid-cols-1 sm:grid-cols-2 gap-4 h-[85vh]">
            <div className="overview  place-content-center">
              <h1 className="text-[46px] sm:text-[60px] text-primary font-extrabold">
                One more friend
              </h1>
              <h2 className="text-[28px] sm:text-[46px] text-primary font-bold">
                Thousands more fun!
              </h2>
              <p className="text-[12px] sm:text-base text-primary font-medium max-w-[500px]">
                Having a pet means you have more joy, a new friend, a happy
                person who will always be with you to have fun. We have 200+
                different pets that can meet your needs!
              </p>
              <div className="action-btn my-5 flex gap-4 items-center">
                <button className="btn-primary-outline flex gap-2 items-center py-3 px-5 ">
                  View intro
                  <img
                    src="https://i.ibb.co/SnctKRK/Vector.png"
                    className="h-[20px] w-[20px]"
                    alt=""
                  />
                </button>
                <a href="" className="btn-primary py-3 px-5  ">
                  Explore Now
                </a>
              </div>
            </div>
            <div className="thumbnail relative">
              <img
                src="https://i.ibb.co/YQ3FfpP/Rectangle-1.png"
                alt=""
                className="absolute bottom-0 z-[2] w-[95%] mb-[-52px] sm:mb-[-150px]"
              />
              <img
                src="https://i.ibb.co/PwXbtGm/Rectangle-2.png"
                alt=""
                className="absolute bottom-0 z-[3] mb-[-28px] sm:mb-[-100px]"
              />
              <img
                src="https://i.ibb.co/gmnKHbj/good-humored-woman-holds-dog-laughing-pink-background-emotional-sort-haired-girl-grey-hoodie-poses-w.png"
                alt=""
                className="absolute bottom-0 z-[5]"
              />
            </div>
          </div>
        </div>
      </section>
      <section className="my-[60px]">
        <div className="container mx-auto px-5 xl:px-0 ">
          <div className="sm:flex block items-center justify-between">
            <div className="left">
              <p className="text-base font-medium">Whats new?</p>
              <h1 className="font-bold text-2xl text-primary">
                Take a look at some of our pets
              </h1>
            </div>
            <div className="right sm:block hidden">
              <a
                href=""
                className="btn-primary-outline flex gap-2 items-center py-3 px-5"
              >
                View more
                <img
                  src="https://i.ibb.co/hXqR2Z0/Vector.png"
                  className="-rotate-90"
                  alt=""
                />
              </a>
            </div>
          </div>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-5 my-10">
            {[1, 2, 3, 4, 5, 6, 7, 8].map((item) => (
              <div className="card p-4 rounded-[12px] mb-[35px]" key={item}>
                <div className="card-header">
                  <img
                    src="https://i.ibb.co/fdcwK6c/image-2.png"
                    className="aspect-[1/1] rounded-xl"
                    alt=""
                  />
                </div>
                <div className="card-body mt-4">
                  <h1 className="text-base font-bold">
                    MO231 - Pomeranian White
                  </h1>
                  <ul className="flex items-center gap-5 my-2">
                    <li className="flex items-center gap-5 text-[#667479] text-[12px]">
                      Gene: <span className="font-bold">Male</span>
                    </li>
                    <li className="flex items-center gap-5 text-[#667479] text-[12px]">
                      Age: <span className="font-bold">02 months </span>
                    </li>
                  </ul>
                  <h1 className="text-sm font-bold">6.900.000 VND</h1>
                </div>
              </div>
            ))}
          </div>
          <div className="right sm:hidden block mt-[-50px]">
            <a
              href=""
              className="btn-primary-outline justify-center flex gap-2 items-center py-3 px-5"
            >
              View more
              <img
                src="https://i.ibb.co/hXqR2Z0/Vector.png"
                className="-rotate-90"
                alt=""
              />
            </a>
          </div>
        </div>
      </section>

      <section className="baner first-baner my-[30px] xl:mx-0 mx-5">
        <div className="box relative overflow-hidden  container mx-auto  bg-primary rounded-2xl">
          <div className="grid grid-cols-1 xl:grid-cols-2 ">
            <div className="left xl:order-1 order-2 relative">
              <img
                src="https://i.ibb.co/y4LgtTK/horizontal-shot-adult-girl-jeans-overalls-kissing-cute-puppy-while-raising-it-air-young-girl-being-l.png"
                alt=""
                className="z-[1] relative"
              />
            </div>
            <div className="right xl:order-2 order-1 relative grid place-content-center text-right">
              <div className="container mx-auto px-10 ">
                <h1 className="text-[60px] text-primary font-extrabold">
                  One more friend
                </h1>
                <h2 className="text-[46px] text-primary font-bold">
                  Thousands more fun!
                </h2>
                <p className="text-bas text-primary mx-auto mr-0 my-5 font-medium max-w-[500px]">
                  Having a pet means you have more joy, a new friend, a happy
                  person who will always be with you to have fun. We have 200+
                  different pets that can meet your needs!
                </p>
                <div className="action-btn my-5 flex gap-4 justify-end items-center">
                  <button className="btn-primary-outline flex gap-2 items-center py-3 px-5 ">
                    View intro
                    <img
                      src="https://i.ibb.co/SnctKRK/Vector.png"
                      className="h-[20px] w-[20px]"
                      alt=""
                    />
                  </button>
                  <a href="" className="btn-primary py-3 px-5  ">
                    Explore Now
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="my-[60px]">
        <div className="container mx-auto px-5 xl:px-0 ">
          <div className="sm:flex block items-center justify-between">
            <div className="left">
              <p className="text-base font-medium">
                Hard to choose right products for your pets?
              </p>
              <h1 className="font-bold text-2xl text-primary">Our Products</h1>
            </div>
            <div className="right sm:block hidden">
              <a
                href=""
                className="btn-primary-outline flex gap-2 items-center py-3 px-5"
              >
                View more
                <img
                  src="https://i.ibb.co/hXqR2Z0/Vector.png"
                  className="-rotate-90"
                  alt=""
                />
              </a>
            </div>
          </div>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-5 my-10">
            {[1, 2, 3, 4, 5, 6, 7, 8].map((item) => (
              <div className="card p-4 rounded-[12px] mb-[35px]" key={item}>
                <div className="card-header">
                  <img
                    src="https://i.ibb.co/fdcwK6c/image-2.png"
                    className="aspect-[1/1] rounded-xl"
                    alt=""
                  />
                </div>
                <div className="card-body mt-4">
                  <h1 className="text-base font-bold">
                    MO231 - Pomeranian White
                  </h1>
                  <ul className="flex items-center gap-5 my-2">
                    <li className="flex items-center gap-5 text-[#667479] text-[12px]">
                      Product: <span className="font-bold">Costume</span>
                    </li>
                    <li className="flex items-center gap-5 text-[#667479] text-[12px]">
                      Age: <span className="font-bold">1.5kg </span>
                    </li>
                  </ul>
                  <h1 className="text-sm font-bold mb-5">6.900.000 VND</h1>
                  <label className="flex tag gap-3 items-center text-primary font-bold text-sm bg-[#FCEED5] rounded-lg p-2">
                    <img
                      src="https://i.ibb.co/n1S48BT/Frame.png"
                      className="h-[25px] w-[25px]"
                      alt=""
                    />
                    <span className="flex items-center gap-2">
                      Free Toy & Free Shaker
                    </span>
                  </label>
                </div>
              </div>
            ))}
          </div>
          <div className="right sm:hidden block mt-[-50px]">
            <a
              href=""
              className="btn-primary-outline justify-center flex gap-2 items-center py-3 px-5"
            >
              View more
              <img
                src="https://i.ibb.co/hXqR2Z0/Vector.png"
                className="-rotate-90"
                alt=""
              />
            </a>
          </div>
        </div>
      </section>

      <section className="my-[60px]">
        <div className="container mx-auto px-5 xl:px-0 ">
          <div className="sm:flex block items-center justify-between">
            <div className="left">
              <h1 className="font-medium text-2xl text-primary">
                Proud to be part of
                <span className="font-bold">Pet Sellers</span>
              </h1>
            </div>
            <div className="right sm:block hidden">
              <a
                href=""
                className="btn-primary-outline flex gap-2 items-center py-3 px-5"
              >
                View all our sellers
                <img
                  src="https://i.ibb.co/hXqR2Z0/Vector.png"
                  className="-rotate-90"
                  alt=""
                />
              </a>
            </div>
          </div>
          <ul className="flex flex-wrap items-center gap-10 justify-between">
            <li>
              <img
                src="https://i.ibb.co/DD2DMkv/image-11.png"
                className="aspect-auto"
                alt=""
              />
            </li>
            <li>
              <img
                src="https://i.ibb.co/CK4kw7k/image-10.png"
                className="aspect-auto"
                alt=""
              />
            </li>
            <li>
              <img
                src="https://i.ibb.co/j5N5FK3/image-9.png"
                className="aspect-auto"
                alt=""
              />
            </li>
            <li>
              <img
                src="https://i.ibb.co/WKFtHBd/image-8.png"
                className="aspect-auto"
                alt=""
              />
            </li>
            <li>
              <img
                src="https://i.ibb.co/P9cv3Pb/image-6.png"
                className="aspect-auto"
                alt=""
              />
            </li>
            <li>
              <img
                src="https://i.ibb.co/ch5tRHG/image-4.png"
                className="aspect-auto"
                alt=""
              />
            </li>
          </ul>
          <div className="right sm:hidden block mt-[50px]">
            <a
              href=""
              className="btn-primary-outline justify-center flex gap-2 items-center py-3 px-5"
            >
              View more
              <img
                src="https://i.ibb.co/hXqR2Z0/Vector.png"
                className="-rotate-90"
                alt=""
              />
            </a>
          </div>
        </div>
      </section>

      <section className="baner second-baner my-[30px] xl:mx-0 mx-5">
        <div className="box relative overflow-hidden  container mx-auto px-5 xl:px-0 bg-[#FFB775] rounded-2xl">
          <div className="grid grid-cols-1 lg:grid-cols-2 ">
            <div className="left relative grid place-content-center text-left">
              <div className="container mx-auto px-5 sm:px-10 ">
                <h1 className="text-[60px] sm:justify-start justify-center flex gap-3 items-center text-primary font-extrabold">
                  Adoption
                  <img
                    src="https://i.ibb.co/3mT0vgX/fontisto-paw.png"
                    className="h-[45px]"
                    alt=""
                  />
                </h1>
                <h2 className="text-[46px] text-primary font-bold">
                  We need help. so do they.
                </h2>
                <p className="text-bas text-primary mx-auto ml-0 my-5 font-medium max-w-[300px]">
                  Adopt a pet and give it a home, it will be love you back
                  unconditionally.
                </p>
                <div className="action-btn my-5 flex gap-4 justify-start items-center">
                  <a href="" className="btn-primary py-3 px-5  ">
                    Explore Now
                  </a>
                  <button className="btn-primary-outline flex gap-2 items-center py-3 px-5 ">
                    View intro
                    <img
                      src="https://i.ibb.co/SnctKRK/Vector.png"
                      className="h-[20px] w-[20px]"
                      alt=""
                    />
                  </button>
                </div>
              </div>
            </div>
            <div className="right relative">
              <img
                src="https://i.ibb.co/7pwRN2g/pngegg-4-1.png"
                alt=""
                className="z-[1] relative"
              />
            </div>
          </div>
        </div>
      </section>

      <section className="my-[60px]">
        <div className="container mx-auto px-5 xl:px-0 ">
          <div className="sm:flex block items-center justify-between">
            <div className="left">
              <p className="text-base font-medium">You already know ?</p>
              <h1 className="font-bold text-2xl text-primary">
                Useful pet knowledge
              </h1>
            </div>
            <div className="right sm:block hidden">
              <a
                href=""
                className="btn-primary-outline flex gap-2 items-center py-3 px-5"
              >
                View more
                <img
                  src="https://i.ibb.co/hXqR2Z0/Vector.png"
                  className="-rotate-90"
                  alt=""
                />
              </a>
            </div>
          </div>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 gap-5 my-10">
            {[1, 2, 3].map((item) => (
              <div className="card p-4 rounded-[12px] mb-[35px]" key={item}>
                <div className="card-header">
                  <img
                    src="https://i.ibb.co/fdcwK6c/image-2.png"
                    className="aspect-[1/1] rounded-xl"
                    alt=""
                  />
                </div>
                <div className="card-body mt-4">
                  <label className="text-[10px] font-bold text-white bg-[#00A7E7] py-1 px-3 rounded-3xl">
                    Pet knowledge
                  </label>
                  <h1 className="text-base font-bold text-[#00171F] my-4">
                    What is a Pomeranian? How to Identify Pomeranian Dogs
                  </h1>
                  <p className="text-sm font-normal">
                    The Pomeranian, also known as the Pomeranian (Pom dog), is
                    always in the top of the cutest pets. Not only that, the
                    small, lovely, smart, friendly, and skillful circus dog
                    breed.
                  </p>
                </div>
              </div>
            ))}
          </div>
          <div className="right sm:hidden block mt-[-50px]">
            <a
              href=""
              className="btn-primary-outline justify-center flex gap-2 items-center py-3 px-5"
            >
              View more
              <img
                src="https://i.ibb.co/hXqR2Z0/Vector.png"
                className="-rotate-90"
                alt=""
              />
            </a>
          </div>
        </div>
      </section>
      <Footer />
    </main>
  );
}

export default App;
